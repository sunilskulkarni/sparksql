from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import StringType, IntegerType
from pyspark.sql import SparkSession
import sys

####
# 1. Setup (10 points): Download the gbook file and write a function to load it in an RDD & DataFrame
####

# RDD API
# Columns:
# 0: place (string), 1: count1 (int), 2: count2 (int), 3: count3 (int)


# Spark SQL - DataFrame API


####
# 5. Joining (10 points): The following program construct a new dataframe out of 'df' with a much smaller size.
####



# Now we are going to perform a JOIN operation on 'df2'. Do a self-join on 'df2' in lines with the same #'count1' values and see how many lines this JOIN could produce. Answer this question via DataFrame API and #Spark SQL API
# Spark SQL API

# output: 9658

try:
    sc = SparkContext()
    sqlContext = SQLContext(sc)
    spark = SparkSession.builder.appName("CS498-MP8").getOrCreate()

    schema = StructType([
        StructField("word", StringType()),
        StructField("count1", IntegerType()),
        StructField("count2", IntegerType()),
        StructField("count3", IntegerType())
    ])
    df = spark.read.csv("gbooks", header='false', schema=schema, ignoreLeadingWhiteSpace='true',
                        ignoreTrailingWhiteSpace='true', sep="\t")
    df2 = df.select("word", "count1").distinct().limit(1000)
    df2.createOrReplaceTempView('gbooks2')
    #df.createOrReplaceTempView("gbooks")
    # df.createOrReplaceTempView("so_tags")
    # sqlContext.sql("show tables").show()
    # sqlContext.sql("select * from gbooks limit 10").show()
    result = sqlContext.sql("SELECT count(*) AS pageCount FROM gbooks2 a inner join gbooks2 b on a.count1 = b.count1 ").collect()
    print(result[0].pageCount)
except:
    print(sys.exc_info()[0])
    pass
finally:
    spark.stop()