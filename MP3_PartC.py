from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import StringType, IntegerType
from pyspark.sql import SparkSession
import sys







    ####
    # 1. Setup (10 points): Download the gbook file and write a function to load it in an RDD & DataFrame
    ####

    # RDD API
    # Columns:
    # 0: place (string), 1: count1 (int), 2: count2 (int), 3: count3 (int)

    # Spark SQL - DataFrame API

    ####
    # 3. Filtering (10 points) Count the number of appearances of word 'ATTRIBUTE'
    ####

    # Spark SQL

    # +--------+
    # |count(1)|
    # +--------+
    # |     201|
    # +--------+
try:
    sc = SparkContext()
    sqlContext = SQLContext(sc)
    spark = SparkSession.builder.appName("CS498-MP8").getOrCreate()

    schema = StructType([
        StructField("word", StringType()),
        StructField("count1", IntegerType()),
        StructField("count2", IntegerType()),
        StructField("count3", IntegerType())
    ])
    df = spark.read.csv("gbooks", header='false', schema=schema, ignoreLeadingWhiteSpace='true',
                        ignoreTrailingWhiteSpace='true', sep="\t")
    # print(df.count())
    df.createOrReplaceTempView("gbooks")
    # df.createOrReplaceTempView("so_tags")
    # sqlContext.sql("show tables").show()
    # sqlContext.sql("select * from gbooks limit 10").show()
    sqlContext.sql("SELECT count(1) FROM gbooks where word in ('ATTRIBUTE')").show()

except:
    print(sys.exc_info()[0])
    pass
finally:
    spark.stop()


