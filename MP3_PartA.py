from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import StringType, IntegerType
from pyspark.sql import SparkSession
import sys




    ####
    # 1. Setup (10 points): Download the gbook file and write a function to load it in an RDD & DataFrame
    ####
    # rdd=sc.textFile("gbooks")
    # mappedRdd= rdd.map(lambda x:x.split("\t"))
    # print(mappedRdd.take(5))
    # #df = mappedRdd.toDF()
    # df = mappedRdd.map(
    #     lambda x: Row(word=x[0], count1=int(x[1]), count2=int(x[2]), count3=x[3])).toDF()
    # df.printSchema()

    # spark = SparkSession.builder.appName("CS498-MP8").getOrCreate()
    # schema = StructType([
    #     StructField("word", StringType()),
    #     StructField("count1", IntegerType()),
    #     StructField("count2", IntegerType()),
    #     StructField("count3", IntegerType())
    # ])
    # csv_2_df = spark.read.csv("gbooks", header='false', schema=schema)
    # csv_2_df.printSchema()
    #print(csv_2_df.count())

    # RDD API
    # Columns:
    # 0: place (string), 1: count1 (int), 2: count2 (int), 3: count3 (int)
    # Spark SQL - DataFrame API
try:
    sc = SparkContext()
    sqlContext = SQLContext(sc)
    spark = SparkSession.builder.appName("CS498-MP8").getOrCreate()

    schema = StructType([
        StructField("word", StringType()),
        StructField("count1", IntegerType()),
        StructField("count2", IntegerType()),
        StructField("count3", IntegerType())
    ])
    df = spark.read.csv("gbooks", header='false', schema=schema, ignoreLeadingWhiteSpace='true',
                        ignoreTrailingWhiteSpace='true', sep="\t")
    df.printSchema()

except:
    print(sys.exc_info()[0])
    pass
finally:
    spark.stop()


