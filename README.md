# SparkSQL

SparkSQL examples

# Setup SparkSQL
https://spark.apache.org/sql/

# Schema
Download the gbook file from the reepo into current directory (around 1.8g),rename it to "gbooks", and write a function to load it in an RDD & DataFrame. Print your DataFrame's schema.

`spark-submit MP3_PartA.py`

Your output should contain:

```
root
 |-- word: string (nullable = true)
 |-- count1: integer (nullable = true)
 |-- count2: integer (nullable = true)
 |-- count3: integer (nullable = true)
 ```

# Counting
How many lines does the RDD contains? Answer this question via Spark SQL api.
`spark-submit MP3_PartB.py`
```
+--------+
|count(1)|
+--------+
|86618505|
+--------+
```

# Filtering
Count the number of appearances of word 'ATTRIBUTE'.
`spark-submit MP3_PartC.py`
```
+--------+
|count(1)|
+--------+
|     201|
+--------+
```

# MapReduce
List the three most frequent 'word' with their count of appearances.
`spark-submit MP3_PartD.py`
```
 +---------+--------+
 |     word|count(1)|
 +---------+--------+
 |  all_DET|     425|
 | are_VERB|     425|
 |about_ADP|     425|
 +---------+--------+
 only showing top 3 rows
 ```

 # Joining
The following program construct a new DataFrame out of 'df' with a much smaller size.
```df2 = df.select("word", "count1").distinct().limit(1000)
   df2.createOrReplaceTempView('gbooks2') # Register table name for SQL
 ```

 Now we are going to perform a JOIN operation on 'df2'. Do a self-join on 'df2' in lines with the same 'count1' values and see how many lines this JOIN could produce. Answer this question via DataFrame API and Spark SQL API

 `spark-submit MP3_PartE.py`
 ```
 +--------+
|count(1)|
+--------+
|   10246|
+--------+
```





