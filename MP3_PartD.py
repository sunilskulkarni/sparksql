from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import StringType, IntegerType
from pyspark.sql import SparkSession
import sys



####
# 1. Setup (10 points): Download the gbook file and write a function to load it in an RDD & DataFrame
####

# RDD API
# Columns:
# 0: place (string), 1: count1 (int), 2: count2 (int), 3: count3 (int)


# Spark SQL - DataFrame API


####
# 4. MapReduce (10 points): List the three most frequent 'word' with their count of appearances
####

# Spark SQL

# There are 18 items with count = 425, so could be different 
# +---------+--------+
# |     word|count(1)|
# +---------+--------+
# |  all_DET|     425|
# | are_VERB|     425|
# |about_ADP|     425|
# +---------+--------+

#https://spark.apache.org/docs/latest/sql-getting-started.html#starting-point-sparksession
#https://spark.apache.org/docs/latest/api/python/pyspark.sql.html?highlight=read%20csv

try:
    sc = SparkContext()
    sqlContext = SQLContext(sc)
    spark = SparkSession.builder.appName("CS498-MP8").getOrCreate()

    schema = StructType([
        StructField("word", StringType()),
        StructField("count1", IntegerType()),
        StructField("count2", IntegerType()),
        StructField("count3", IntegerType())
    ])
    df = spark.read.csv("gbooks", header='false', schema=schema, ignoreLeadingWhiteSpace='true',
                        ignoreTrailingWhiteSpace='true', sep="\t")
    # print(df.count())
    df.createOrReplaceTempView("gbooks")
    # df.createOrReplaceTempView("so_tags")
    # sqlContext.sql("show tables").show()
    # sqlContext.sql("select * from gbooks limit 10").show()
    sqlContext.sql("SELECT word, count(1) FROM gbooks group by word order by count(1) desc").show(3)

except:
    print(sys.exc_info()[0])
    pass
finally:
    spark.stop()